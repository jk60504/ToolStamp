// jQuery onReady
$(function(){
    stamp_app.init_stamp();
    datepicker_app.init_datepicker();
    $('#input_datepicker').datetimepicker({
        locale: 'zh-TW',
        format: 'YYYY-MM-DD HH:mm:ss',
        defaultDate: datepicker_app.objDate.getTime()
    }).on('dp.change', function(e){
        datepicker_app.strDate = $(this).val();
    }); 
});

// create new vue class
var stamp_app = new Vue({
    //
    el: '#stamp_app',

    // vue bind data
    data: {
        objDate: new Date(),
        strStamp: '',
        hisStamp: [],
    },

    // vue bind event method
    methods: {
        init_stamp: function(){
            this.objDate = new Date();
            this.strStamp = this.objDate.getTime()
                                        .toString()
                                        .substr(0, 10);
        },

        convert_stamp: function(){
            let match, month, datetime = '';

            // get time stamp value ( 10 number )
            match = this.strStamp.match(/(\d)/g);
            if(match.length !== 10){
                return false;
            }

            // create date object of time stamp value
            this.objDate = new Date(Number(match.join('')) * 1e3);

            // create date string - format: yyyy-mm-dd hh:ii:ss
            month = Number(this.objDate.getMonth()) + 1;
            datetime += this.objDate.getFullYear()+'-';
            datetime += ('00'+month).slice(-2)+'-';
            datetime += ('00'+this.objDate.getDate()).slice(-2)+' ';
            datetime += ('00'+this.objDate.getHours()).slice(-2)+':';
            datetime += ('00'+this.objDate.getMinutes()).slice(-2)+':';
            datetime += ('00'+this.objDate.getSeconds()).slice(-2);

            // push date
            this.hisStamp.push({
                str_stamp: match.join(''),
                str_datetime: datetime
            });
        }
    }
});

// create new vue class
let datepicker_app = new Vue({
    //
    el: '#datepicker_app',
    
    //
    data: {
        objDate: new Date(),
        strDate: '',
        hisDate: new Array()
    },

    //
    methods: {
        //
        init_datepicker: function(){
            this.objDate = new Date();
            this.strDate = this.objDate.getFullYear()+'-';
            let month = (Number(this.objDate.getMonth()) + 1);
            this.strDate += ('00'+month).slice(-2)+'-';
            this.strDate += ('00'+this.objDate.getDate()).slice(-2)+' ';
            this.strDate += ('00'+this.objDate.getHours()).slice(-2)+':';
            this.strDate += ('00'+this.objDate.getMinutes()).slice(-2)+':';
            this.strDate += ('00'+this.objDate.getSeconds()).slice(-2);
        },

        convert_datepicker: function(){
            let match, stamp;
            match = this.strDate.match(/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/);
            if( match.length!==7 ){ console.log(match); return false; }

            this.objDate = new Date(
                Number(match[1]), 
                (Number(match[2])-1), 
                Number(match[3]), 
                Number(match[4]), 
                Number(match[5]), 
                Number(match[6]), 
            );

            stamp = this.objDate.getTime();
            stamp = stamp.toString().substr(0,10);

            this.hisDate.push({
                str_date: match[0],
                str_stamp: stamp
            });
        },

        convert_datepickerST: function(){
            let match, stamp;
            match = this.strDate.match(/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/);
            if( match.length!==7 ){ 
                console.log(match); 
                return false; 
            }

            this.objDate = new Date(
                Number(match[1]), 
                (Number(match[2])-1), 
                Number(match[3]),
                0, 0, 0
            );

            stamp = this.objDate.getTime();
            stamp = stamp.toString().substr(0, 10);

            //
            this.hisDate.push({
                str_date: match[1]+'-'+match[2]+'-'+match[3]+' 00:00:00',
                str_stamp: stamp
            });
        },

        convert_datepickerED: function(){
            let match, stamp;
            match = this.strDate.match(/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})$/);
            if( match.length!==7 ){ 
                console.log(match); 
                return false; 
            }

            this.objDate = new Date(
                Number(match[1]), 
                (Number(match[2])-1), 
                Number(match[3]),
                23, 59, 59
            );

            stamp = this.objDate.getTime();
            stamp = stamp.toString().substr(0, 10);

            //
            this.hisDate.push({
                str_date: match[1]+'-'+match[2]+'-'+match[3]+' 23:59:59',
                str_stamp: stamp
            });
        }
    },
});

